# Jenkins Basics

A simple Maven-based Java project that demonstrates the use of a `Jenkinsfile`.

## Usage

At the end of this exercise, you should be able to accomplish the following:

- Any repository push to BitBucket will trigger Jenkins for a build (Webhook)
- Jenkins will build and test your code (Jenkinsfile)
- Jenkins will submit your code results and code coverage to SonarQube.
- The entire pipeline is made available for viewing at *Blue Ocean*, the new Jenkins
  user interface.

See the diagram below for the entire process:

![Armory CI/CD](files/Armory%20CI_CD.png)

## Links

- [Blue Ocean for JenkinsBasics](https://jenkins.dev.novare.com.hk/blue/organizations/jenkins/jenkins-basics/)
- [SonarQube Page for JenkinsBasics](https://sonar.dev.novare.com.hk/dashboard?id=com.novare.armory%3Ajenkinsbasics)

## Pipeline Basics

To set up the pipeline, you'll need to do these tasks:

- Authorize Jenkins by submitting the public key to your BitBucket project.
- Set up a webhook to trigger builds for new repository pushes.
- Make use of the build notification indicator using Jenkins.

### The Default Approach: Manual Blue Ocean

Because of our huge `novaretechnologies` team repository, it's advised to follow
these instructions instead of the Jenkins *BitBucket Cloud Integration*.

**Repository Setup**

1. With a BitBucket repository in `novaretechnologies`, prepare your *Jenkinsfile*.
2. Check this repository to have an idea how a `Jenkinsfile` looks like. The default
   used here should work for most `Maven-based` Java projects.

**Create your Pipeline**

1. Create a new project in [Jenkins Blue Ocean](https://jenkins.dev.novare.com.hk/blue/pipelines)
2. Select `Git` (not *BitBucket Cloud*)
3. Paste the *Remote URL* of this repository to the provided box.
4. Blue Ocean will provide you an *SSH Public Key*. 
5. Proceed to the BitBucket repository settings and add it under 
   the *Access Keys* section. `https://bitbucket.org/novaretechnologies/YOUR-PROJECT-NAME/admin/access-keys/`
  - If the above step fails, try going to the *SSH Keys* link under *Pipelines*, 
    then go back to Jenkins.
  - No need to actually enable *BitBucket Pipelines.* -- BitBucket itself sometimes
    doesn't recognize access keys unless you do this.
    
    
**Automated Builds**

After doing all of the above, you can configure a *webhook* so that BitBucket will notify
Jenkins when new commits are pushed.

1. Set up a webhook in BitBucket so that the project will trigger a build every time changes are mede.
  - Under *Name*, enter: `Jenkins BitBucket Webhook`
  - Under *URL*, enter: `https://jenkins.dev.novare.com.hk/bitbucket-hook/` 
  **The trailing slash should be included.**
  - Save your changes.
- Ensure that your `Jenkinsfile` has a BitBucket Plugin trigger.

```groovy
    triggers {
        bitbucketPush()
    }
```
- Do a *manual build* so that Jenkins has a copy of your *Jenkinsfile*. Future subsequent pushes 
  to your repository should now trigger Jenkins.
  
**Status Notifiers**

You can add the `bitbucketStatusNotify()` steps to your *Jenkinsfile* wherever appropriate.

Here's a basic example of everything so far:

```groovy
triggers {
    bitbucketPush()
}
stages {
    stage('Initialization') {
        steps {
            bitbucketStatusNotify(buildState: 'INPROGRESS')
        }
    }
    stage('Build') {
        steps {
            sh 'mvn -B -DskipTests clean install package'
        }
    }
    stage('Test') {
        steps {
            steps {
                sh 'mvn test'
            }
        }
        post {
            always {
                junit 'target/surefire-reports/*.xml'
            }
        }
    }
}
post {
    success {
        bitbucketStatusNotify(buildState: 'SUCCESSFUL')
    }
    failure {
        bitbucketStatusNotify(buildState: 'FAILED')

    }
}
```

**Testing and Code Quality**

At this point, you'll notice that your code will run in Jenkins, but you won't have
code quality feedback.

Your next step is to prepare the repository for automated tests and code quality checks.

You can accomplish this through your application's preferred testing framework, 
code coverage runner (if any) and submitting the results to *SonarQube*.

*Please consult the SonarQube documentation for collecting code coverage and results
for other project types. This repository only demonstrates a Java setup*

*Java: JaCoCo + SonarQube*

Add these settings to your `pom.xml`.

Doing these will allow `mvn` to run code coverage `org.jacoco:jacoco-maven-plugin:prepare-agent` and 
submit the results to Sonar `org.sonarsource.scanner.maven:sonar-maven-plugin:3.5.0.1254:sonar`

```
 <properties>
      <jacoco.version></jacoco.version>
      <sonar.maven.version></sonar.maven.version>
 </properties>
 
 ...

 <build>
        <!-- Testing and Code Coverage -->
        <plugins>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>${jacoco.version}</version>
            </plugin>
        </plugins>


        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.sonarsource.scanner.maven</groupId>
                    <artifactId>sonar-maven-plugin</artifactId>
                    <version>${sonar-maven.version}</version>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-surefire-plugin</artifactId>
                    <configuration>
                        <runOrder>random</runOrder>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.jacoco</groupId>
                    <artifactId>jacoco-maven-plugin</artifactId>
                    <configuration>
                        <append>true</append>
                    </configuration>
                    <executions>
                        <execution>
                            <id>agent-for-ut</id>
                            <goals>
                                <goal>prepare-agent</goal>
                            </goals>
                        </execution>
                        <execution>
                            <id>agent-for-it</id>
                            <goals>
                                <goal>prepare-agent-integration</goal>
                            </goals>
                        </execution>
                        <execution>
                            <id>jacoco-site</id>
                            <phase>verify</phase>
                            <goals>
                                <goal>report</goal>
                            </goals>
                        </execution>
                    </executions>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

```

After that, you'll need to inclde changes to your *Jenkinsfile*:

```groovy

stage('Test') {
    steps {
        withSonarQubeEnv('SonarQube') {
            sh "mvn clean org.jacoco:jacoco-maven-plugin:prepare-agent test package org.sonarsource.scanner.maven:sonar-maven-plugin:3.5.0.1254:sonar"
        }
    }
    post {
        always {
            junit 'target/surefire-reports/*.xml'
        }
    }
}

```

## Resources

BitBucket integration is made possible thanks to these plugins:

- [BitBucket Status Notifier Plugin](https://wiki.jenkins.io/display/JENKINS/Bitbucket%2BCloud%2BBuild%2BStatus%2BNotifier%2BPlugin)
- [BitBucket Plugin](https://wiki.jenkins.io/display/JENKINS/BitBucket%2BPlugin)

You can always refer to the Jenkins homepage for Jenkins info, and the SonarQube wiki for Sonar-related items.

- [Using a Jenkinsfile](https://jenkins.io/doc/book/pipeline/jenkinsfile/)
- [SonarQube Jenkins Plugin](https://docs.sonarqube.org/display/SCAN/Analyzing+with+SonarQube+Scanner+for+Jenkins#AnalyzingwithSonarQubeScannerforJenkins-AnalyzingwithSonarQubeScannerforMavenorGradle)
- [SonarScanning Examples](https://github.com/SonarSource/sonar-scanning-examples)
- [Seeing Coverage](https://docs.sonarqube.org/display/SONAR/Seeing%2BCoverage) and [JaCoCo + SonarQube](https://docs.sonarqube.org/display/PLUG/Usage%2Bof%2BJaCoCo%2Bwith%2BSonarJava)
