package com.novare.armory.jenkinsbasics;

/**
 * A simple box.
 * <p>
 * It has a height, width and said values are
 * immutable upon instantiation.
 *
 * @author jerieljan
 */
public class Box {

    private final int height;
    private final int width;

    /**
     * Creates a new box.
     *
     * @param height the height of the box.
     * @param width  the width of the box.
     */
    public Box(int height, int width) {
        this.height = height;
        this.width = width;
    }

    /**
     * Retrieves the height of the box.
     *
     * @return
     */
    public int getHeight() {
        return height;
    }

    /**
     * Retrieves the width of the box.
     *
     * @return
     */
    public int getWidth() {
        return width;
    }
}
