package com.novare.armory.jenkinsbasics;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Date: 2018-10-02
 * Time: 3:43 PM
 *
 * @author jerieljan
 */
public class BoxTest {

    @Test
    public void simpleBox() {
        int randomHeight = (int) Math.random();
        int randomWidth = (int) Math.random();
        Box test = new Box(randomHeight, randomWidth);

        assertNotNull(test);
        assertEquals(randomHeight, test.getHeight());
        assertEquals(randomWidth, test.getWidth());

    }

}